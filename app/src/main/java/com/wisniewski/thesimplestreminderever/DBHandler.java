package com.wisniewski.thesimplestreminderever;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Handler for database
 */
public class DBHandler extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "AlarmsDB";

    // Alarms table name
    private static final String TABLE_ALARMS = "alarms";

    // Alarms Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "name";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_ACTIVE = "active";
    private static final String KEY_YEAR = "year";
    private static final String KEY_MONTH = "month";
    private static final String KEY_DAY = "day";
    private static final String KEY_HOUR = "hour";
    private static final String KEY_MINUTE = "minute";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ALARMS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ALARMS + "("
                + KEY_ID + " INTEGER," + KEY_TITLE + " TEXT,"
                + KEY_DESCRIPTION + " TEXT," + KEY_ACTIVE + " INTEGER,"
                + KEY_YEAR + " INTEGER," + KEY_MONTH + " INTEGER,"
                + KEY_DAY + " INTEGER," + KEY_HOUR + " INTEGER,"
                + KEY_MINUTE + " INTEGER" + ")";
        db.execSQL(CREATE_ALARMS_TABLE);
    }//onCreate()

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALARMS);
        // Creating tables again
        onCreate(db);
    }//onUpgrade()

    /**
     * Adds new alarm
     * @param alarm Alarm to add
     */
    public void addAlarm(Alarm alarm) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, alarm.getId());
        values.put(KEY_YEAR, alarm.getmYear());
        values.put(KEY_MONTH, alarm.getmMonth());
        values.put(KEY_DAY, alarm.getmDay());
        values.put(KEY_HOUR, alarm.getmHour());
        values.put(KEY_MINUTE, alarm.getmMinute());
        values.put(KEY_TITLE, alarm.getmTitle());
        values.put(KEY_DESCRIPTION, alarm.getmDescription());
        values.put(KEY_ACTIVE, alarm.getmActive());

        // Inserting Row
        db.insert(TABLE_ALARMS, null, values);
        db.close(); // Closing database connection
    }//addAlarm()

    /**
     * Getting all alarms
     * @return alarms list
     */
    public List<Alarm> getAllAlarms() {
        List<Alarm> alarmsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ALARMS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Alarm alarm = new Alarm();
                alarm.setId(Integer.parseInt(cursor.getString(0)));
                alarm.setmTitle(cursor.getString(1));
                alarm.setmDescription(cursor.getString(2));
                alarm.setmActive(Integer.parseInt(cursor.getString(3)));
                alarm.setmYear(Integer.parseInt(cursor.getString(4)));
                alarm.setmMonth(Integer.parseInt(cursor.getString(5)));
                alarm.setmDay(Integer.parseInt(cursor.getString(6)));
                alarm.setmHour(Integer.parseInt(cursor.getString(7)));
                alarm.setmMinute(Integer.parseInt(cursor.getString(8)));

                // Adding contact to list
                alarmsList.add(alarm);
            } while (cursor.moveToNext());
        }//if

        cursor.close();
        // return contact list
        return alarmsList;
    }//getAllAlarms()

    /**
     * Deleting an alarm by object
     * @param alarm Alarm to delete
     */
    public void deleteAlarm(Alarm alarm) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ALARMS, KEY_ID + " = ?",
                new String[] { String.valueOf(alarm.getId()) });
        db.close();
    }//deleteAlarm()

    /**
     * Deleting an alarm by ID
     * @param id identifier of alarm to delete
     */
    public void deleteAlarm(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ALARMS, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }//deleteAlarm()
}//DBHandler
