package com.wisniewski.thesimplestreminderever;

/**
 * Constants used by multiple classes in this package
 */
public final class Constants {
    public static final String TITLE = "com.wisniewski.thesimplestreminderever.extra.TITLE";
    public static final String DESCRIPTION = "com.wisniewski.thesimplestreminderever.extra.DESCRIPTION";
    public static final String ALARM_ID = "com.wisniewski.thesimplestreminderever.extra.ALARM_ID";
}
