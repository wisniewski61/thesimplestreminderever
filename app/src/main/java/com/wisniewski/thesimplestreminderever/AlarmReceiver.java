package com.wisniewski.thesimplestreminderever;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import java.util.Calendar;

/**
 * AlarmReceiver class
 */
public class AlarmReceiver extends WakefulBroadcastReceiver {
    // The app's AlarmManager, which provides access to the system alarmReceiver services.
    private AlarmManager alarmMgr;
    // The pending intent that is triggered when the alarmReceiver fires.
    private PendingIntent alarmIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        // Get title and description
        String title = intent.getStringExtra(Constants.TITLE);
        String description = intent.getStringExtra(Constants.DESCRIPTION);
        int id = intent.getIntExtra(Constants.ALARM_ID, 0);
        // Create notification service
        Intent service = new Intent(context, NotificationService.class);
        service.putExtra(Constants.TITLE, title);
        service.putExtra(Constants.DESCRIPTION, description);
        service.putExtra(Constants.ALARM_ID, id);

        startWakefulService(context, service);
    }

    /**
     * Sets new alarm
     * @param context Application context
     * @param alarm Alarm to set
     */
    public void setAlarm(Context context, Alarm alarm) {
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        // set title and description
        intent.putExtra(Constants.TITLE, alarm.getmTitle());
        intent.putExtra(Constants.DESCRIPTION, alarm.getmDescription());
        intent.putExtra(Constants.ALARM_ID, alarm.getId());
        alarmIntent = PendingIntent.getBroadcast(context, alarm.getId(), intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        // Set the alarmReceiver's trigger time
        calendar.set(Calendar.YEAR, alarm.getmYear());
        calendar.set(Calendar.MONTH, alarm.getmMonth());
        calendar.set(Calendar.DAY_OF_MONTH, alarm.getmDay());
        calendar.set(Calendar.HOUR_OF_DAY, alarm.getmHour());
        calendar.set(Calendar.MINUTE, alarm.getmMinute());

        // Set the alarmReceiver to fire at approximately time
        alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
    }//setAlarm()

    /**
     * Cancel current alarm
     * @param context Application context
     */
    public void cancelAlarm(Context context) {
        // If the alarmReceiver has been set, cancel it.
        if (alarmMgr!= null) {
            alarmMgr.cancel(alarmIntent);
        }

        // Disable {@code SampleBootReceiver} so that it doesn't automatically restart the
        // alarmReceiver when the device is rebooted.
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }//cancelAlarm()
}//AlarmReceiver
