package com.wisniewski.thesimplestreminderever;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Activity to display information about alarm/notification
 */
public class AlarmInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_info);

        Intent intent = getIntent();
        String title = intent.getStringExtra(Constants.TITLE);
        String description = intent.getStringExtra(Constants.DESCRIPTION);
        int id = intent.getIntExtra(Constants.ALARM_ID, 0);

        // delete alarm from DB
        new DBHandler(this).deleteAlarm(id);

        TextView titleText = (TextView) findViewById(R.id.AlarmInfoTitle);
        if (titleText != null) {
            titleText.setText(title);
        }//if

        TextView descriptionText = (TextView) findViewById(R.id.AlarmInfoDescription);
        if (descriptionText != null) {
            descriptionText.setText(description);
        }//if
    }//onCreate()
}//AlarmInfoActivity
