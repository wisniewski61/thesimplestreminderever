package com.wisniewski.thesimplestreminderever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.wisniewski.thesimplestreminderever.AlarmReceiver;

import java.util.List;

/**
 * This class adds alarmReceiver after (re-)boot.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
        {
            DBHandler db = new DBHandler(context);
            List<Alarm> alarms = db.getAllAlarms();

            for (Alarm alarm :
                    alarms) {
                if(alarm.getmActive() == 1) {
                    AlarmReceiver alarmReceiver = new AlarmReceiver();
                    alarmReceiver.setAlarm(context, alarm);
                } else {
                    db.deleteAlarm(alarm);
                }//if-else
            }//for
        }//if
    }//onReceive()
}//BootReceiver
