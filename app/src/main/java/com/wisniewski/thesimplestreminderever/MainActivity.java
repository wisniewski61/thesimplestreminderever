package com.wisniewski.thesimplestreminderever;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * MainActivity
 */
public class MainActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_RECEIVE_BOOT_COMPLETED = 1;
    //private final String TAG = this.getClass().getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        askForBootPermission();
        enableBootReceiver(this);
    }//onCreate()

    @Override
    protected void onResume() {
        super.onResume();

        updateActiveAlarmsDisplay();
    }//onResume()

    /**
     * Request for permissions
     */
    private void askForBootPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_BOOT_COMPLETED)
                != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECEIVE_BOOT_COMPLETED},
                        MY_PERMISSIONS_REQUEST_RECEIVE_BOOT_COMPLETED);

                // MY_PERMISSIONS_REQUEST_RECEIVE_BOOT_COMPLETED is an
                // app-defined int constant. The callback method gets the
                // result of the request.
        }//if
    }//askForBootPermission()

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, "Permission denied - app could not work properly.", Toast.LENGTH_SHORT).show();
                }
                //return;
            }//1

            // other 'case' lines to check for other
            // permissions this app might request
        }//switch
    }//onRequestPermissionsResult()

    /**
     * Enable {@code SampleBootReceiver} to automatically restart the alarmReceiver when the
     * device is rebooted.
     *
     * @param context Application context
     */
    private void enableBootReceiver(Context context) {
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }//enableBootReceiver()

    /**
     * Update displayed active alarms
     */
    private void updateActiveAlarmsDisplay() {
        DBHandler db = new DBHandler(this);
        List<Alarm> alarms = db.getAllAlarms();
        StringBuilder activeAlarms = new StringBuilder();

        for(Alarm alarm : alarms) {
            StringBuilder msg = new StringBuilder()
                    .append(" Title: ").append(alarm.getmTitle())
                    .append("\n Description: ").append(alarm.getmDescription())
                    .append("\n Time: ").append(alarm.getmYear())
                    .append("-").append(alarm.getmMonth()+1)
                    .append("-").append(alarm.getmDay())
                    .append(" ").append(alarm.getmHour())
                    .append(":").append(alarm.getmMinute()).append("\n\n");

            activeAlarms.append(msg);
        }//for

        TextView activeAlarmsTextView = (TextView) findViewById(R.id.activeAlarmsText);
        if (activeAlarmsTextView != null) {
            activeAlarmsTextView.setText(activeAlarms);
        }//if
    }//updateActiveAlarmsDisplay()

    /**
     * addNewAlarmButton onclick function
     * @param view Pressed button
     */
    public void addNewAlarmPressed(View view) {
        Intent intent = new Intent(this, AddingAlarmActivity.class);
        startActivity(intent);
    }//addNewAlarmPressed()
}//MainActivity
