package com.wisniewski.thesimplestreminderever;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Activity for adding alarm view
 */
public class AddingAlarmActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_WAKE_LOCK = 2;
    // Variables for Datapicker
    private static int mYear, mMonth, mDay, mHour, mMinute;
    private static boolean isDatePicked = false;

    private static TextView mDateDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_alarm);

        initDateAndTime();
        askForWakeLockPermission();
    }//onCreate()

    private void askForWakeLockPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WAKE_LOCK)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            //if (ActivityCompat.shouldShowRequestPermissionRationale(this,
               //     Manifest.permission.WAKE_LOCK)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            //} else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WAKE_LOCK},
                        MY_PERMISSIONS_REQUEST_WAKE_LOCK);

                // MY_PERMISSIONS_REQUEST_WAKE_LOCK is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            //}//if-else
        }//if
    }//askForWakeLockPermission()

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                } else {
                    // permission denied, boo!
                    Toast.makeText(AddingAlarmActivity.this, "Permission denied - this functionality couldn't work.", Toast.LENGTH_SHORT).show();
                    // close activity
                    finish();
                }
                //return;
            }//1

            // other 'case' lines to check for other
            // permissions this app might request
        }//switch
    }//onRequestPermissionsResult()

    /**
     * Gets current date & time and displays it
     */
    private void initDateAndTime() {
        mDateDisplay = (TextView) findViewById(R.id.dateLabel);

        if (!isDatePicked) {
            // get the current date & time
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);
        }//if

        // display the current date
        updateDisplay();
    }//initDateAndTime()

    /**
     * Update displeyed date and time
     */
    private static void updateDisplay() {
        mDateDisplay.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(mMonth + 1).append("-")
                        .append(mDay).append("-")
                        .append(mYear).append(" ")
                        .append(mHour).append(":")
                        .append(mMinute).append(" "));
    }//updateDisplay()

    /**
     * setAlarmButton on click method
     *
     * @param view pressed button
     */
    public void setAlarmPressed(View view) {
        AlarmReceiver alarmReceiver = new AlarmReceiver();

        EditText title = (EditText) findViewById(R.id.titleText);
        if (isEmpty(title)) {
            Toast.makeText(getApplicationContext(), "Title cannot be empty!", Toast.LENGTH_SHORT).show();
            return;
        }//if
        String titleStr = "";
        if (title != null) {
            titleStr = title.getText().toString();
        }

        String descriptionStr = "";
        EditText description = (EditText) findViewById(R.id.descriptionText);
        if (description != null) {
            if (!isEmpty(description)) {
                descriptionStr = description.getText().toString();
            }//if
        }//if

        Alarm alarm = new Alarm(mYear, mMonth, mDay, mHour, mMinute, titleStr, descriptionStr);
        // set alarm
        alarmReceiver.setAlarm(this, alarm);
        // add alarm to database
        DBHandler db = new DBHandler(this);
        db.addAlarm(alarm);

        Toast.makeText(getApplicationContext(), "Alarm added", Toast.LENGTH_SHORT).show();

        // close activity
        finish();
    }//setAlarmPressed()

    /**
     * Return true if the EditText field is empty.
     *
     * @param etText EditText field to check
     * @return true/false
     */
    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() <= 0;
    }//isEmpty

    /**
     * DatePicker
     */
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }//onCreateDialog()

        public void onDateSet(DatePicker view, int year, int month, int day) {
            mYear = year;
            mMonth = month;
            mDay = day;
            isDatePicked = true;

            updateDisplay();
        }//onDateSet()
    }//DatePickerFragment

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
        showTimePickerDialog();
    }//showDatePickerDialog()

    /**
     * TimePicker
     */
    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }//onCreateDialog()

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mHour = hourOfDay;
            mMinute = minute;

            updateDisplay();
        }//onTimeSet()
    }//TimePickerFragment

    public void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }//showTimePickerDialog()
}//AddingAlarmActivity
