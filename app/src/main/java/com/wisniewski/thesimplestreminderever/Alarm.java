package com.wisniewski.thesimplestreminderever;

import java.util.UUID;

/**
 * Alarm class
 */
public class Alarm {
    private int id, mYear, mMonth, mDay, mHour, mMinute;
    private String mTitle, mDescription;
    private int mActive; // is alarmReceiver active?

    Alarm(int year, int month, int day, int hour, int minute, String title, String description) {
        id = generateUniqueId();
        mYear = year;
        mMonth = month;
        mDay = day;
        mHour = hour;
        mMinute = minute;
        mTitle = title;
        mDescription = description;
        mActive = 1; // default true
    }//Alarm()

    Alarm() {}

    /**
     * Generate unique identifier
     * @return unique ID
     */
    public int generateUniqueId() {
        UUID idOne = UUID.randomUUID();
        String str=""+idOne;
        int uid=str.hashCode();
        String filterStr=""+uid;
        str=filterStr.replaceAll("-", "");
        return Integer.parseInt(str);
    }

    public int getId() {
        return id;
    }

    public int getmYear() {
        return mYear;
    }

    public void setmYear(int mYear) {
        this.mYear = mYear;
    }

    public int getmMonth() {
        return mMonth;
    }

    public void setmMonth(int mMonth) {
        this.mMonth = mMonth;
    }

    public int getmDay() {
        return mDay;
    }

    public void setmDay(int mDay) {
        this.mDay = mDay;
    }

    public int getmHour() {
        return mHour;
    }

    public void setmHour(int mHour) {
        this.mHour = mHour;
    }

    public int getmMinute() {
        return mMinute;
    }

    public void setmMinute(int mMinute) {
        this.mMinute = mMinute;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getmActive() {
        return mActive;
    }

    public void setmActive(int mActive) {
        this.mActive = mActive;
    }
}//Alarm
